<?php
// $Id: $

function commerce_platnosci_pl_close_transaction() {
  /*
   sta³a opis
   %transId% identyfikator nowej transakcji utworzonej w aplikacji Platnosci.pl
   %posId% wartosci pos id
   %payType% wartosci pay type
   %sessionId% wartosci session id
   %amountPS% wartosci amount - jako separator kropka
   %amountCS% wartosci amount - jako separator przecinek
   %orderId% wartosci order id
   %error% numer b³edu zgodnie z tabelka 2.1 (s. 3), jest wykorzystywany tylko przy


P³atnosci.pl configuration URLs.
Unsuccessful return URL 
http://www.drupal-commerce.pl/commerce_platnosci_pl/close_transaction/error?trans_id=%transId%&pos_id=%posId%&pay_type=%payType%&session_id=%sessionId%&amount=%amountPS%&order_id=%orderId%&error=%error%
http://www.drupal-commerce.pl/checkout/%orderId%/payment/back/%sessionId%?trans_id=%transId%&pos_id=%posId%&pay_type=%payType%&session_id=%sessionId%&amount=%amountPS%&order_id=%orderId%&error=%error%

Successful return URL
http://www.drupal-commerce.pl/commerce_platnosci_pl/close_transaction?trans_id=%transId%&pos_id=%posId%&pay_type=%payType%&session_id=%sessionId%&amount=%amountPS%&order_id=%orderId%
http://www.drupal-commerce.pl/checkout/%orderId%/payment/return/%sessionId%?trans_id=%transId%&pos_id=%posId%&pay_type=%payType%&session_id=%sessionId%&amount=%amountPS%&order_id=%orderId%
Report URL http://www.drupal-commerce.pl/commerce_platnosci_pl/online
   */
  $output = '';
  $error_id = isset($_REQUEST['error']) ? $_REQUEST['error'] : 0;
  $order_id = isset($_REQUEST['order_id']) ? $_REQUEST['order_id'] : 0;
  $session_id = isset($_REQUEST['session_id']) ? $_REQUEST['session_id'] : 0;
  
  $a_session_id = explode(':', $session_id, 2);
  $payment_redirect_key = (count($a_session_id)==2) ? $a_session_id[1]: '';

  $output .= '<strong>_REQUEST</strong><pre>'.print_r($_REQUEST, 1).'</pre>';
  $output .= '<strong>a_session_id</strong><pre>'.print_r($a_session_id, 1).'</pre>';
  $output .= '<strong>payment_redirect_key</strong><pre>'.print_r($payment_redirect_key, 1).'</pre>';

  $url_error = "checkout/$order_id/payment/back/$payment_redirect_key";
  $url_ok = "checkout/$order_id/payment/return/$payment_redirect_key";
 
  $output .= l($url_error, $url_error)."<br/>";
  $output .= l($url_ok, $url_ok)."<br/>";


  if ($error_id) {
    drupal_set_message(t('Transaction unsuccessful').': '. _commerce_platnosci_pl_get_error_description($error_id), 'error');
    drupal_goto($url_error);
    
  }
  else {
    drupal_goto($url_ok);
  }
  return $output;
}



/**
 * menu callback for raports platnosc.pl
 *
 */
function commerce_platnosci_pl_online() {
  $pos_id = isset($_POST['pos_id']) ? check_plain($_POST['pos_id']): 0;
  $session_id = isset($_POST['session_id']) ? check_plain($_POST['session_id']): 0;
  $ts = isset($_POST['ts']) ? check_plain($_POST['ts']): 0;
  $sig = isset($_POST['sig']) ? check_plain($_POST['sig']): 0;

  // debug
  //print '<pre>'.print_r($_POST, 1).'<pre>';

  $watchdog_variables = array('@session_id' => $session_id);
  $watchdog_urls = '';
  
  if (!$session_id) {
    watchdog('commerce_platnosci_pl', 'ERROR empty session_id', $watchdog_variables, WATCHDOG_WARNING);
    print t('ERROR empty session_id');
    exit();
  }

  $a_session_id = explode(':', $session_id, 2);
  $order_id = intval($a_session_id[0]);
  $payment_redirect_key = (count($a_session_id)==2) ? $a_session_id[1]: '';  
  
  if (!$order_id) {
    watchdog('commerce_platnosci_pl', 'ERROR order_id', $watchdog_variables, WATCHDOG_WARNING);
    print t('ERROR order_id');
    exit();
  }
  
  $order = commerce_order_load($order_id);
  $payment_method_instance_id = $order->data['payment_method'];
  $payment_method = commerce_payment_method_instance_load($payment_method_instance_id);
  // debug
  //print "payment_method_instance_id: $payment_method_instance_id<br/>";
  //print '<strong>payment_method</strong><pre>'.print_r($payment_method, 1).'</pre>';
  //print '<strong>order</strong><pre>'.print_r($order, 1).'</pre>';


  if ($pos_id != $payment_method['settings']['pos_id']) {
    // bad pos_id (shop)
    $watchdog_variables['@pos_id'] = $pos_id;
    watchdog('commerce_platnosci_pl', 'ERROR bad pos_id: @pos_id.', $watchdog_variables, WATCHDOG_WARNING);
    print t('ERROR BAD pos_id: @pos_id.', $watchdog_variables);
    exit();
  }

  if (md5( $pos_id . $session_id . $ts . $payment_method['settings']['md5_2']) != $sig) {
    $to_sig = $pos_id . $session_id . $ts . $payment_method['settings']['md5_2'];
    $sig_local = md5( $pos_id . $session_id . $ts . $payment_method['settings']['md5_2']);

    $watchdog_variables['@pos_id'] = $pos_id;
    $watchdog_variables['@ts'] = $ts;
    $watchdog_variables['@commerce_platnosci_pl_md5_2'] = $payment_method['settings']['md5_2'];
    $watchdog_variables['@sig'] = $sig;
    $watchdog_variables['@sig_local'] = $sig_local;
    $watchdog_variables['@to_sig'] = $to_sig;
    watchdog('commerce_platnosci_pl',
      'Platnosci.pl transaction @session_id sig error. to_sig=[@to_sig] (@pos_id, @session_id, @ts, commerce_platnosci_pl_md5_2, @sig, @sig_local)'
      , $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
    print t('ERROR: sig, see to log');
    exit();
  }

  $url = "https://www.platnosci.pl/paygw/UTF/Payment/get";

  $ts = time();
  $sig = md5( $pos_id . $session_id . $ts . $payment_method['settings']['md5']);
  $data = "&pos_id=". $pos_id ."&session_id=". urlencode($session_id) ."&ts=". $ts ."&sig=". $sig;
  
  $assoc_options = array(
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8'), 
    'method' => 'POST', 
    'data' => $data,
  );
  $result = drupal_http_request($url, $assoc_options);

  if ($result->code != 200) {
    $watchdog_variables['@serialize'] = serialize($result);
    watchdog('commerce_platnosci_pl', 'Error sending confirmation to Platnosci.pl: @session_id @serialize', $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
    print t('ERROR: @code, see to drupal log', array('@code' => $result->code));
    exit;
  }

  $remote = _commerce_platnosci_pl_parse_xml_raport($result->data);
  //print '<strong>remote:</strong><pre>'.print_r($remote, 1).'</pre>';
  //exit;
  if ($remote['status'] == 'OK') {
    $watchdog_urls = implode(', ', array(l(t('view in platnosci.pl'), 
      _commerce_platnosci_pl_details_url($remote['trans']['id']))));

    
    if ($remote['trans']['pos_id'] != $payment_method['settings']['pos_id']) {
      $watchdog_variables['@pos_id'] = $pos_id;
      $watchdog_variables['@remote_pos_id'] = $remote['trans']['pos_id'];
      watchdog('commerce_platnosci_pl', 'Error błędny numer POS_ID: @pos_id<>@remote_pos_id', $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
      print t('ERROR: bad @POS_ID', array('@pos_id' => $remote['trans']['pos_id']));
      exit;
    }
    $sig = md5($remote['trans']['pos_id'] . $remote['trans']['session_id'] .
      $remote['trans']['order_id'] . $remote['trans']['status'] . 
      $remote['trans']['amount'] . htmlspecialchars_decode($remote['trans']['desc']) .
      $remote['trans']['ts'] . $payment_method['settings']['md5_2']);

    if ($remote['trans']['sig'] != $sig) {
      watchdog('commerce_platnosci_pl', 'Error bledny podpis statusu transakcji', $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
      print t('ERROR: błędny podpis');
      exit;
    }

    if ($remote['trans']['status']) {  //--- payment status recognized
      //$remote_id = $remote['trans']['id'];
      //$pos_id = $remote['trans']['pos_id'];
      $desc = $remote['trans']['desc'];
      $ts = $remote['trans']['ts'];
      $sig = $remote['trans']['sig'];

      $status_description = _commerce_platnosci_pl_get_status_description($remote['trans']['status']);

      $watchdog_variables['@status_id'] = $remote['trans']['status'];
      $watchdog_variables['@status_description'] = $status_description;
      watchdog('commerce_platnosci_pl', 'transaction: @session_id, status_description: @status_description, status_id: @status_id', $watchdog_variables, WATCHDOG_INFO, $watchdog_urls);

      foreach (commerce_payment_transaction_load_multiple(
        array(), 
        array('remote_id' => $remote['trans']['id'])) as $transaction) {
      }

      if(empty($transaction)) {
        $transaction = commerce_payment_transaction_new('platnosci_pl', $order->order_id);
        $transaction->created = time();
        print 'empty</br>';
      }
      $transaction->instance_id = $payment_method['instance_id'];

      $transaction->remote_id = $remote['trans']['id'];
      $transaction->amount = $remote['trans']['amount']*10;//commerce_currency_decimal_to_amount($ipn['mc_gross'], $ipn['mc_currency']);
      $transaction->currency_code = 'PL';
      $transaction->payload[REQUEST_TIME] = $_POST;

      $transaction->changed = time();

      $transaction->message = t('The payment status @status_description.'. $watchdog_urls, $watchdog_variables);
      $transaction->remote_status = $status_description;
      //print '<strong>transaction</strong><pre>'.print_r($transaction, 1).'</pre>';

      switch ($remote['trans']['status']) {
        case '1': // new
          $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
          break;
        case '4': // Started
          $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
          break;
        case '5': // Awaiting reception
          $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
          break;
        case '2': // Cancelled
        case '3': // Rejected
        case '6': // Negative authorization
        case '7': // Payment rejected
          $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
          break;
        case '99': // Payment received
          $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
          break;
        case '888': // Incorrect status
          $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
          break;
        default:
          $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
          break;
      }
      //print '<strong>transaction</strong><pre>'.print_r($transaction, 1).'</pre>';
      commerce_payment_transaction_save($transaction);
      echo "OK";
      exit;
    }
    else {
      watchdog('commerce_platnosci_pl', 'unkown status', $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
      print t('ERROR: unkown status');
      exit;
    }
  }
  else {
    // notification will be resend by platnosci.pl
    if (!empty($remote['error']['nr'])) {
      $watchdog_variables['@code'] = $session_id;
      $watchdog_variables['@desc'] = _commerce_platnosci_pl_get_error_description($remote['error']['nr']);
      $watchdog_variables['@message'] = $remote['error']['message'];
      watchdog('commerce_platnosci_pl', 'Error sending confirmation to Platnosci.pl: @session_id @code, @desc, @message', $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
      print t('ERROR: @code, @desc, @message. See to drupal log', $watchdog_variables);
      exit;
    }
    else {
      $watchdog_variables['@serialize'] = serialize($result);
      watchdog('commerce_platnosci_pl', 'Error sending confirmation to Platnosci.pl: @session_id @serialize', $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
      print t('ERROR: Unknown, see Watchdog1');
      exit;
    }
  }
  watchdog('commerce_platnosci_pl', 'ERROR: Unknown, see Watchdog', $watchdog_variables, WATCHDOG_WARNING, $watchdog_urls);
  print t('ERROR: Unknown, see Watchdog2');
  exit;
}


function _commerce_platnosci_pl_parse_xml_raport($xml_raport) {
  $remote = array();
  
  $xml_obj = simplexml_load_string($xml_raport);
  $data = get_object_vars($xml_obj);
  $remote['status'] = isset($data['status']) ? $data['status'] : 'UNKOWN';
  $trans = get_object_vars($xml_obj->trans);
  foreach($trans as $key=>$val) {
    if (is_object($val)) {
      $remote['trans'][$key] = '';
    }
    else {
      $remote['trans'][$key] = $val;
    }
  }
  $trans = get_object_vars($xml_obj->error);
  foreach($trans as $key=>$val) {
    if (is_object($val)) {
      $remote['error'][$key] = '';
    }
    else {
      $remote['error'][$key] = $val;
    }
  }
  return $remote;
}


// ----

/**
 * List Type Status
 *
 * @return array
 */
function _commerce_platnosci_pl_status_types() {
  return array(
    '0' => t('Not started'),
    '1' => t('New'),
    '2' => t('Cancelled'),
    '3' => t('Rejected'),
    '4' => t('Started'),
    '5' => t('Awaiting reception'),
    '6' => t('Negative authorization'),
    '7' => t('Payment rejected'),
    '99' => t('Payment received'),
    '888' => t('Incorrect status'),
  );
}

/**
 * Error description list
 *
 * @return array
 */
function _commerce_platnosci_pl_error_description() {
  return array(
    '100' => t('No pos id variable.'),
    '101' => t('No session id variable.'),
    '102' => t('No ts variable.'),
    '103' => t('No sig variable.'),
    '104' => t('No pos id variable.'),
    '105' => t('No client ip variable.'),
    '106' => t('No first name variable.'),
    '107' => t('No last name variable.'),
    '108' => t('No street variable.'),
    '109' => t('No city variable.'),
    '110' => t('No post code variable.'),
    '111' => t('No amount variable.'),
    '112' => t('Incorrect bank account number.'),
    '113' => t('No e-mail variable.'),
    '114' => t('No phone number variable.'),
    '200' => t('Other temporary error.'),
    '201' => t('Other temporary database error.'),
    '202' => t('Stated pos id is blocked.'),
    '203' => t('Forbidden pay type value for stated pos id.'),
    '204' => t('Stated payment method (pay type value) is temporarily blocked for pos id, e.g. payment gateway is under maintenance.'),
    '205' => t('Transaction amount is lower than minimum allowed value.'),
    '206' => t('Transaction amount is higher than maximum allowed value.'),
    '207' => t('Transactions value per client in last time period has been exceeded.'),
    '208' => t('Pos is working in ExpressPayment mode which was not activated yet (awaiting Helpdesk authorization).'),
    '209' => t('Incorrect pos id number or pos auth key.'),
    '500' => t('Transaction does not exist.'),
    '501' => t('No authorization for this transaction.'),
    '502' => t('Transaction previously initiated.'),
    '503' => t('Authorization for transaction was already made.'),
    '504' => t('Transaction previously cancelled.'),
    '505' => t('Transaction already transferred to receive.'),
    '506' => t('Transaction already received.'),
    '507' => t('Error returning funds to the client.'),
    '599' => t('Incorrect transaction state - please contact site administrator.'),
    '999' => t('Other critical error - please contact site administrator.'),
  );
}

function _commerce_platnosci_pl_get_status_description($status_id) {
  $status_types = _commerce_platnosci_pl_status_types();
  return isset($status_types[$status_id]) ? $status_types[$status_id] : t('unknown');
}

function _commerce_platnosci_pl_get_error_description($error_id) {
  $error_description = _commerce_platnosci_pl_error_description();
  return isset($error_description[$error_id]) ? $error_description[$error_id] : t('unknown');
}

function _commerce_platnosci_pl_details_url($trans_id) {
  return 'https://www.platnosci.pl/paygw/adm/showTransDetails.do?transId='. $trans_id;
}

